const mongoURI = "mongodb://localhost:27017/Employee";

let mongoose = require('mongoose');
const {userSchema, EmployeeSchema } = require('./schema')

mongoose.connect(mongoURI, { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => { console.log("connection established with mongodb server online"); })
    .catch(err => {
        console.log("error while connection", err)
});

const LoginModel = mongoose.model('User', userSchema);
const EmpModel = mongoose.model('Employee',EmployeeSchema);



module.exports = {
    LoginModel,
    EmpModel,
}
