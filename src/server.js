require('dotenv').config()

const express = require('express')
const app = express()
const jwt = require('jsonwebtoken')
const bcrypt = require('bcrypt');
const { LoginModel,EmpModel} = require('./connector');
const cors = require('cors');

app.use(express.json())


const isNullOrUndefined = val => val===null || val === undefined;

const SALT = 3;

let refreshTokens = [];

app.post('/login', async(req, res) => {
    // Authenticate User
      const {username,password} = req.body;
      const existingUser = await LoginModel.findOne({Email : username});
  
      if(isNullOrUndefined(existingUser))
      {
          res.status(401).send({err : "UserName does not exists"});
      }
      else
      {
          
          const hashpass = existingUser.PassWord;
          //console.log(existingUser);
          //console.log(hashpass);
          if(bcrypt.compareSync(password,hashpass))
          {
              
              const user = { name: username }
  
              const accessToken = generateAccessToken(user)
              const refreshToken = jwt.sign(user, process.env.REFRESH_TOKEN_SECRET)
              
              refreshTokens.push(refreshToken)
              
              const responseData = {success: "LoggedIn",accessToken: accessToken, refreshToken: refreshToken};
              res.status(200).send(responseData);
          }
          else
          {
              res.status(401).send({err : "UserName/Password Inccorrect"});
          }   
      }
  });
  
  //generate new acess token for user 
  function generateAccessToken(user) {
      return jwt.sign(user, process.env.ACCESS_TOKEN_SECRET, { expiresIn: '120s' })
  }
  
  // refresh token
  app.post('/token', (req, res) => {
      const refreshToken = req.body.token
      if (refreshToken == null) return res.sendStatus(401)
      if (!refreshTokens.includes(refreshToken)) return res.sendStatus(403)
      jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
        if (err) return res.sendStatus(403)
        const accessToken = generateAccessToken({ name: user.name })
        res.json({ accessToken: accessToken })
      })
  })
    
  // logout and delete generated token
  app.delete('/logout', (req, res) => {
      refreshTokens = refreshTokens.filter(token => token !== req.body.token)
      res.sendStatus(204)
  })

//registration of new user
app.post('/signup', async(req,res)=>{

    const {firstname,lastname,username,password,phone,occupation,gender} = req.body;
    const existingUser = await LoginModel.findOne({Email : username});

    //console.log(existingUser);

    if(isNullOrUndefined(existingUser))
    {

        const hashpass = bcrypt.hashSync(password,SALT);
        const newUser = new LoginModel({
            FirstName : firstname,
            LastName : lastname,
            Email : username,
            PassWord : hashpass,
            Phone : phone,
            Occupation : occupation,
            Gender : gender,
        });


        await newUser.save();
        res.status(201).send({success: "Account Successfuly Created"});
    }
    else
    {
        res.status(400).send({err: `Email ${username} already Exists please choose another`});
    }
});


//authentcation of jwt token
function authenticateToken(req, res, next) {
    const authHeader = req.headers['authorization']
    const token = authHeader; //&& authHeader.split(' ')[1]
    if (token == null) return res.sendStatus(401)
  
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, (err, user) => {
      console.log(err)
      if (err) return res.sendStatus(403)
      req.user = user
      next()
    })
}


//get all employees
app.get('/employee', authenticateToken, async(req, res) => {
    const empData = await EmpModel.find();

    res.send(empData);

})

//create new employee
app.post('/employee',authenticateToken,async(req,res)=>{
    const {fname,lname,email,Designation} = req.body;
    const existingEmp = await EmpModel.findOne({email : email});
    //console.log(existingEmp);

    if(isNullOrUndefined(existingEmp))
    {
        const newEmp = new EmpModel({
            fname: fname,
            lname: lname,
            email: email,
            Designation: Designation
        });


        await newEmp.save();
        res.status(201).send({success: "Employee details added successfully"});
    }
    else
    {
        res.status(400).send({err: `Employee with ${email} is already exists`});
    }
});


//update employee records

app.put('/employee/:id',authenticateToken,async(req,res)=>{

    EmpModel.findOneAndUpdate({ _id: req.params.id }, req.body, { new: true }, (err, doc) => {
        if (!err) 
        { 
            res.send({success : "Updated Successfully"}); 
        }
        else 
        {
            res.status(500).send({err : "Something went wrong"});
        }
    });
});


//get single record
app.get('/employee/:id', (req, res) => {
    EmpModel.findById(req.params.id, (err, doc) => {
        if (!err) {
            res.send ({
                employee: doc
            });
        }
    });
});


//delete single record
app.delete('/employee/:id', (req, res) => {
    EmpModel.findByIdAndRemove(req.params.id, (err, doc) => {
        if (!err) {
            res.send({success: "Employee deleted Successfully"});
        }
        else { console.log('Error in employee delete :' + err); }
    });
});


app.listen(3000,()=>{
    console.log("Server is running");
})
