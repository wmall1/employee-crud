const mongoose = require('mongoose');


const userSchema = new mongoose.Schema({
    FirstName : {
        type: String,
        required: true,
    },
    LastName : {
        type: String,
        required: true,
    },
    Email : {
        type: String,
        required: true,
    },
    PassWord : {
        type: String,
        required: true,
    },
    Phone : {
        type: String,
        required: true,
    },
    Occupation : {
        type: String,
        required: true,
    },
    Gender : {
        type: String,
        required: true,
    },
});


const EmployeeSchema = new mongoose.Schema({
    fname: {
        type: String,
        required: true,
    },
    lname: {
        type: String,
        required: true,
    },
    email: {
        type: String,
        required: true,
    },
    Designation: {
        type: String,
        required: true,
    },
})





module.exports = {
    userSchema,
    EmployeeSchema,
}
